# react-learn-project

## Installation:
```
$ git clone https://gitlab.com/vladimirrabtsun/react-learn-project.git
$ cd react-learn-project
$ ./make-lumen-env.sh
$ docker-compose up -d
$ docker-compose exec lumen bash
$$ chmod -R 777 storage
$$ composer install
$$ php artisan migrate
$$ exit
$ docker-compose exec react bash
$$ npm install
$$ exit
```
After, try open `http://localhost/`, if got error, then restart containers:
```
$ docker kill $(docker ps -q)
$ docker-compose up -d
```
## Summary:
`http://localhost/` - react application, frontend.
`http://localhost:8080/` - lumen application, backend.