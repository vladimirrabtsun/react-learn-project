<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TList extends Model {

    protected $fillable = [
        'title',
        'description',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public static $rules = [
        'title' => 'required',
    ];

    public function tasks() {
        return $this->hasMany('App\Task');
    }

}
