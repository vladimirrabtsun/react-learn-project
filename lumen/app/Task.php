<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model {

    protected $fillable = [
        'tasks_list_id',
        'title',
        'description',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'finished_at',
    ];

    public static $rules = [
        'title'         => 'required',
    ];

    public function tasksList() {
        return $this->belongsTo('App\TasksList');
    }
}
