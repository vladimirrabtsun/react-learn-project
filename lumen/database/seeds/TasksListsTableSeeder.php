<?php

use Illuminate\Database\Seeder;

class TasksListsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('t_lists')->insert([
            'title' => 'Job tasks list',
        ]);

        DB::table('t_lists')->insert([
            'title' => 'Home tasks list',
        ]);
    }
}
