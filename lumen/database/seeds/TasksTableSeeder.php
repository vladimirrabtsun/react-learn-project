<?php

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert([
            'title'     => 'First task',
            't_list_id' => '1',
        ]);

        DB::table('tasks')->insert([
            'title' => 'Second task',
            't_list_id' => '1',
        ]);

        DB::table('tasks')->insert([
            'title' => 'Third task',
            't_list_id' => '2',
        ]);

        DB::table('tasks')->insert([
            'title' => 'Fourth task',
            't_list_id' => '2',
        ]);
    }
}
