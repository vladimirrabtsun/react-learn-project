<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\Task;
use App\TList;
use Carbon\Carbon;
use Illuminate\Http\Request;


$router->get('/', function () use ($router) {

    return $router->app->version();
});


// GET ALL LISTS
$router->get('lists', function() {

    $lists = TList::with('tasks')->get()->toArray();

    if (count($lists) == 0) {
        return response('Списков не существует.', 200);
    }

    foreach ($lists as $key => $list) {
        $lists[$key]['finishedTasks'] = [];
        $lists[$key]['unfinishedTasks'] = [];
        foreach ($list['tasks'] as $task) {
            if ($task['finished']) {
                $lists[$key]['finishedTasks'][] = $task;
            } else {
                $lists[$key]['unfinishedTasks'][] = $task;
            }
        }
        unset($lists[$key]['tasks']);
    }

    return response($lists, 200);
});


// GET LIST
$router->get('lists/{listId}', function($listId) {

    $list = TList::find($listId);

    if (is_null($list)) {
        return response('Список не найден.', 404);
    }

    return response($list, 200);
});


// ADD LIST
$router->post('lists', function(Request $request) {

    $this->validate($request, TList::$rules);

    return response(TList::create($request->all()), 200);
});


// UPDATE LIST
$router->put('lists/{listId}', function(Request $request, $listId) {

    $this->validate($request, TList::$rules);

    $list = TList::find($listId);

    if (is_null($list)) {
        return response('Обновляемый список не найден.', 404);
    }

    $list->update($request->all());
    return response('Список обновлен.', 200);
});


// DELETE LIST
$router->delete('lists/{listId}', function($listId) {

    $list = TList::find($listId);

    if (is_null($list)) {
        return response('Удаляемый список не найден.', 404);
    }

    TList::destroy($listId);
    return response('Список удален.', 200);
});


// GET ALL TASKS FOR CURRENT LIST
$router->get('lists/{listId}/tasks', function($listId) {

    $list = TList::find($listId);

    if (is_null($list)) {
        return response('Список не найден.', 404);
    }

    $tasks = $list->tasks;

    if (count($tasks) == 0) {
        return response('Задачи не существуют.', 200);
    }

    return response($tasks, 200);
});


// GET TASK
$router->get('tasks/{taskId}', function($taskId) {

    $task = Task::find($taskId);

    if (is_null($task)) {
        return response('Задача не существует.', 404);
    }

    return response($task, 200);
});


// ADD TASK
$router->post('lists/{listId}/tasks', function(Request $request, $listId) {

    $this->validate($request, Task::$rules);

    $list = TList::find($listId);

    if (is_null($list)) {
        return response('Список не существует.', 404);
    }

    $list->tasks()->save(
        $task = Task::create($request->all())
    );

    return response($task, 200);
});


// UPDATE TASK
$router->put('tasks/{taskId}', function(Request $request, $taskId) {

    $this->validate($request, Task::$rules);

    $task = Task::find($taskId);

    if (is_null($task)) {
        return response('Обновляемая задача не найдена.', 404);
    }

    $task->update($request->all());

    return response('Задача обновлена.', 200);
});


// DELETE TASK
$router->delete('tasks/{taskId}', function($taskId) {

    $task = Task::find($taskId);

    if (is_null($task)) {
        return response('Удаляемая задача не найдена.', 404);
    }

    Task::destroy($taskId);
    return response('Задача удалена.', 200);
});


// FINISH TASK
$router->get('tasks/{taskId}/finish', function($taskId) {

    $task = Task::find($taskId);

    if (is_null($task)) {
        return response('Завершаемая задача не найдена.', 404);
    }

    if ($task->finished) {
        return response('Задача уже завершена.', 404);
    }

    $task->finished = true;
    $task->finished_at = Carbon::now();
    $task->update();
    return response('Задача завершена.', 200);
});


// CONTINUE TASK
$router->get('tasks/{taskId}/continue', function($taskId) {

    $task = Task::find($taskId);

    if (is_null($task)) {
        return response('Продолжаемая задача не найдена.', 404);
    }

    if (!$task->finished) {
        return response('Продолжаемая задача еще не завершена.', 404);
    }

    $task->finished = false;
    $task->update();
    return response('Задача возоблена.');
});