#!/usr/bin/env bash
cp ./lumen/.env.example ./lumen/.env
sed -i -- "s/DB_HOST=.*/DB_HOST=mysql/g" ./lumen/.env
sed -i -- "s/DB_DATABASE=.*/DB_DATABASE=lumen/g" ./lumen/.env
sed -i -- "s/DB_USERNAME=.*/DB_USERNAME=lumen/g" ./lumen/.env