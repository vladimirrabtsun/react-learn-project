import React, { Component } from 'react'
import { connect } from 'react-redux'

import { faPlusCircle  } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class ActionAddList extends Component {

  addList() {
    this.props.onAddList('Test');
  }

  render() {
      return (
        <React.Fragment>

          <button
            className="
              btn
              btn-outline-primary
              m-2
              shadow-sm
            "
            onClick={this.addList.bind(this)}
          >
            <FontAwesomeIcon icon={ faPlusCircle } className="fa-fw" />
            &nbsp;
            Add list
          </button>

        </React.Fragment>
      );
  }
}

export default connect(
  state     => ({
    testStore: state
  }),
  dispatch  => ({
    onAddList: (listTitle) => {
      dispatch({
        type:     'ADD_LIST',
        payload:  listTitle
      })
    }
  })
)(ActionAddList);
