import React, { Component } from 'react'
import { connect } from 'react-redux'

import { faPlusCircle  } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class ActionAddTask extends Component {
    render() {
        return (
            <React.Fragment>

              <button
                className="
                  btn
                  btn-outline-primary
                  m-2
                  float-left
                  shadow-sm
                "
              >
              <FontAwesomeIcon icon={ faPlusCircle } className="fa-fw" />
                &nbsp;
                Add task
              </button>

            </React.Fragment>
        );
    }
}

export default connect(
  state     => ({
    testStore: state
  }),
  dispatch  => ({
    onAddTask: (taskTitle) => {
      dispatch({
        type:     'ADD_TASK',
        payload:  taskTitle
      })
    }
  })
)(ActionAddTask);
