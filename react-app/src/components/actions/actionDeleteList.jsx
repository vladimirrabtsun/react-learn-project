import React, {Component} from 'react'
import {connect} from 'react-redux'
import Rodal from 'rodal'

import {faTrash} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

class ActionDeleteList extends Component {

    constructor(props) {
        super(props);
        this.state = {visible: false};
    }

    show() {
        this.setState({visible: true});
    }

    hide() {
        this.setState({visible: false});
    }

    deleteList() {
        this.props.onDeleteList(this.props.listId);
    }

    render() {
        return (
            <React.Fragment>

                <button
                    className="
                        btn
                        btn-danger
                        btn-sm
                        shadow-sm
                        mt-1
                        mr-0
                        ml-0
                    "
                    onClick={this.show.bind(this)}
                >
                    <FontAwesomeIcon icon={faTrash}/>
                </button>

                <Rodal
                    visible={this.state.visible}
                    onClose={this.hide.bind(this)}
                    showCloseButton={false}
                    width={300}
                    height={170}
                >
                    <div
                        className="
                            mt-3
                            text-center
                        "
                    >
                        Do you really want to delete the list?
                    </div>
                    <div
                        className="
                            mt-4
                        "
                    >
                        <button
                            className="
                                btn
                                btn-light
                                float-left
                                ml-4
                                shadow-sm
                            "
                            onClick={this.hide.bind(this)}
                        >
                            Cancel
                        </button>
                        <button
                            className="
                                btn
                                btn-danger
                                float-right
                                mr-4
                                shadow-sm
                            "
                            onClick={this.deleteList.bind(this)}
                        >
                            <FontAwesomeIcon
                                icon={faTrash}
                                className="
                                    fa-fw
                                "
                            /> Delete
                        </button>
                    </div>
                </Rodal>

            </React.Fragment>
        );
    }
}

export default connect(
    state => ({
        testStore: state
    }),
    dispatch => ({
        onDeleteList: (listId) => {
            fetch('http://localhost:8080/lists/' + listId, {
                method: 'DELETE'
            }).then(
                dispatch({
                    type: 'DELETE_LIST',
                    payload: listId
                })
            )
        }
    })
)(ActionDeleteList);
