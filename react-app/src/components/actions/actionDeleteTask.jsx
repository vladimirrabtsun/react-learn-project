import React, { Component } from 'react';
import { faTrash  } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default class ActionDeleteTask extends Component {
    render() {
        return (
            <React.Fragment>

              <button
                className="
                  btn
                  btn-danger
                  btn-sm
                  ml-1
                  mr-1
                  shadow-sm
                "
              >
                <FontAwesomeIcon icon={ faTrash } />
              </button>

            </React.Fragment>
        );
    }
}
