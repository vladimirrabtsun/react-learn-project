import React, { Component } from 'react'
import { faPen  } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default class ActionEditList extends Component {
    render() {
        return (
          <React.Fragment>

            <button
              className="
                btn
                btn-info
                btn-sm
                shadow-sm
                mr-1
                ml-1
              "
            >
              <FontAwesomeIcon icon={ faPen }  />
            </button>

          </React.Fragment>
        );
    }
}
