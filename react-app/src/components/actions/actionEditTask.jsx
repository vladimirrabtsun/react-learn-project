import React, { Component } from 'react'
import { faPen  } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default class ActionEditTask extends Component {
    render() {
        return (
            <React.Fragment>

              <button
                className="
                  btn
                  btn-info
                  btn-sm
                  ml-1
                  mr-1
                  shadow-sm
                "
              >
                <FontAwesomeIcon icon={ faPen } />
              </button>

            </React.Fragment>
        );
    }
}
