import React, { Component } from 'react'
import { faCheck  } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default class ActionFinishTask extends Component {
    render() {
        return (
            <React.Fragment>

              <button
                className="
                  btn
                  btn-success
                  btn-sm
                  ml-1
                  mr-1
                  shadow-sm
                "
              >
                <FontAwesomeIcon icon={ faCheck } />
              </button>

            </React.Fragment>
        );
    }
}
