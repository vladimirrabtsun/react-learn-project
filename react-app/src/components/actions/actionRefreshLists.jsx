import React, { Component } from 'react';
import { faSync  } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default class ActionRefreshLists extends Component {
    render() {
        return (
          <React.Fragment>

            <button
              className="
                btn
                btn-primary
                m-2
                shadow-sm
              "
            >
              <FontAwesomeIcon icon={ faSync } className="fa-fw" />
              &nbsp;
              Refresh lists
            </button>

          </React.Fragment>
        );
    }
}
