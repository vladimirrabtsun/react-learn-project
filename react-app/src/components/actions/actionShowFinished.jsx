import React, { Component } from 'react';

export default class ActionShowFinished extends Component {
    render() {
        return (
            <React.Fragment>

              <span className="float-right mt-2">
                <input
                  type="checkbox"
                  className="
                    ml-2
                    mt-2
                    mb-2
                  "
                />
                &nbsp;
                <span
                  className="
                    mr-2
                    mt-2
                    mb-2
                  "
                >
                  Show finished tasks
                </span>
              </span>

            </React.Fragment>
        );
    }
}
