import React, { Component } from 'react'
import ActionEditList from './actions/actionEditList'
import ActionDeleteList from './actions/actionDeleteList'

export default class HeaderListActionsBar extends Component {
    render() {
        return (
          <React.Fragment>

            <span className="float-right">
              <ActionEditList />
              <ActionDeleteList />
            </span>


          </React.Fragment>
        );
    }
}
