import React, {Component} from 'react'
import ContentEditable from 'react-sane-contenteditable'

import Task from './task'
import ListActionsBar from './listActionsBar'
import ActionDeleteList from './actions/actionDeleteList'
import connect from "react-redux/es/connect/connect";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";


class List extends Component {

    constructor(props) {
        super(props);
        this.handleMouseHover = this.handleMouseHover.bind(this);
        this.state = {
            isHovering: false,
        };
    }

    handleMouseHover() {
        this.setState(this.toggleHoverState);
    }

    toggleHoverState(state) {
        return {
            isHovering: !state.isHovering,
        };
    }

    handleChange = (ev, value) => {
        this.props.onUpdateListTitle(this.props.list, value);
    };

    render() {
        return (
            <div className="card shadow border-info">
                <div
                    className="
                        card-header
                        lead
                        bg-light
                        pt-0
                        pb-0
                        mt-0
                        mb-0
                    "
                    onMouseEnter={this.handleMouseHover}
                    onMouseLeave={this.handleMouseHover}
                >
                    <ContentEditable
                        tagName="p"
                        className="
                            float-left
                            pt-2
                            pb-2
                            mt-0
                            mb-0
                        "
                        content={this.props.list.title}
                        editable={true}
                        maxLength={140}
                        multiLine={false}
                        onChange={this.handleChange}
                    />
                    <span
                        className="
                            float-right
                            pt-1
                            pb-0
                            mt-0
                            mb-0
                        "
                    >
                        {this.state.isHovering &&
                            <ReactCSSTransitionGroup
                                transitionName="anim"
                                transitionAppear={true}
                                transitionAppearTimeout={5000}
                                transitionEnter={false}
                                transitionLeave={false}
                            >
                                <ActionDeleteList
                                    listId={this.props.list.id}
                                />
                            </ReactCSSTransitionGroup>

                        }
                        {/*<ActionEditList*/}
                        {/*listId={this.props.list.id}*/}
                        {/*/>*/}
                    </span>
                </div>
                <div
                    className="
                        card-body
                        pt-0
                        pb-0
                        pl-1
                        pr-1
                    "
                >
                    <ul className="
                            list-group
                            list-group-flush
                            pt-0
                            pb-0
                            mt-0
                            mb-0
                        "
                    >
                        {this.props.list.unfinishedTasks.map((task) =>
                            <li
                                className="
                                    list-group-item
                                    pt-1
                                    pb-1
                                "
                                key={task.id}
                            >
                                <Task
                                    key={task.id}
                                    task={task}
                                />
                            </li>
                        )}
                    </ul>
                </div>
                <div className="card-footer text-muted">
                    <ListActionsBar/>
                </div>
            </div>
        );
    }
}

export default connect(
    state => ({
        testStore: state
    }),
    dispatch => ({
        onUpdateListTitle: (list, newTitle) => {
            let payload = {
                list: {
                    id: list.id,
                    title: list.title
                },
                newTitle
            }
            fetch('http://localhost:8080/lists/' + list.id + '?title=' + newTitle, {
                method: 'PUT',
                body: JSON.stringify(payload.list)
            }).then(
                dispatch({
                    type: 'UPDATE_LIST_TITLE',
                    payload
                })
            )
        }
    })
)(List);