import React, { Component } from 'react';
import ActionAddTask from './actions/actionAddTask';
import ActionShowFinished from './actions/actionShowFinished';

export default class ListActionsBar extends Component {
    render() {
        return (
            <React.Fragment>
              <ActionAddTask />
              <ActionShowFinished />
            </React.Fragment>
        );
    }
}
