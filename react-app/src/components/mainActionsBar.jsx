import React, { Component } from 'react'
import ActionAddList from './actions/actionAddList'

export default class MainActionsBar extends Component {
    render() {
        return (
            <div className="row mb-3 float-right">
              <ActionAddList />
            </div>
        );
    }
}
