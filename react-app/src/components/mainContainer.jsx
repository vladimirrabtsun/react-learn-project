import React, { Component } from 'react'
import MainDialog from './mainDialog'

export default class MainContainer extends Component {

    render() {
        return (
            <div className="container">
              <MainDialog />
            </div>
        )
    }
}
