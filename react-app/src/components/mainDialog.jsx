import React, {Component} from 'react'
import {connect} from 'react-redux'

import List from './list';
import MainActionsBar from './mainActionsBar'

class MainDialog extends Component {

    componentWillMount() {
        this.props.fetchLists();

    }

    render() {
        return (
            <div className="mt-2 mb-2">
                <div className="row">
                    <MainActionsBar/>
                </div>
                <div className="row">
                    <div>
                        <div className="row">
                            {this.props.testStore.lists.map((list) =>
                                <div
                                    className="
                                        col-sm-12
                                        col-md-12
                                        col-lg-6
                                        mb-3
                                    "
                                    key={list.id}
                                >
                                    <List
                                        key={list.id}
                                        list={list}
                                    />
                                </div>
                            )}
                        </div>
                    </div>
                </div>
                </div>
        );
    }
}

export default connect(
    state => ({
        testStore: state
    }),
    dispatch => ({
        fetchLists: () => {
            fetch('http://localhost:8080/lists')
                .then(res => res.json())
                .then(lists => {
                        dispatch({
                            type:       'FETCH_LISTS',
                            payload:    lists
                        });
                    }
                );
        }
    })
)(MainDialog);
