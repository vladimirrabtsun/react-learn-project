import React, {Component} from 'react';
import TaskActionsBar from './taskActionsBar';
import ContentEditable from "react-sane-contenteditable";
import connect from "react-redux/es/connect/connect";

class Task extends Component {

    constructor(props) {
        super(props);
        this.handleMouseHover = this.handleMouseHover.bind(this);
        this.state = {
            isHovering: false,
        };
    }

    handleMouseHover() {
        this.setState(this.toggleHoverState);
    }

    toggleHoverState(state) {
        return {
            isHovering: !state.isHovering,
        };
    }

    handleChange = (ev, value) => {
        this.props.onUpdateListTitle(this.props.task, value);
    };

    render() {
        return (
            <div
                className="
                    row
                    justify-content-between
                    pt-0
                    pb-0
                    mt-0
                    mb-0
                "
                onMouseEnter={this.handleMouseHover}
                onMouseLeave={this.handleMouseHover}
            >
                <ContentEditable
                    tagName="span"
                    className="
                        float-left
                        pt-0
                        pb-0
                        pl-3
                        mt-1
                        mb-1
                    "
                    content={this.props.task.title}
                    editable={true}
                    maxLength={140}
                    multiLine={false}
                    onChange={this.handleChange}
                />
                <span className="float-right">
                    {this.state.isHovering &&
                        <TaskActionsBar/>
                    }
                </span>
            </div>
        );
    }
}

export default connect(
    state => ({
        testStore: state
    }),
    dispatch => ({
        onUpdateListTitle: (task, newTitle) => {
            let payload = {
                task: {
                    id: task.id,
                    title: task.title
                },
                newTitle
            }
            fetch('http://localhost:8080/tasks/'+task.id+'?title='+newTitle, {
                method: 'PUT',
                body: JSON.stringify(payload.task)
            }).then(
                dispatch({
                    type: 'UPDATE_TASK_TITLE',
                    payload
                })
            )
        }
    })
)(Task);