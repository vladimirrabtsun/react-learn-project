import React, { Component } from 'react'
import ActionFinishTask from './actions/actionFinishTask'
import ActionDeleteTask from './actions/actionDeleteTask'

export default class TaskActionsBar extends Component {
    render() {
        return (
            <React.Fragment>
              <ActionFinishTask />
              {/*<ActionEditTask />*/}
              <ActionDeleteTask />
            </React.Fragment>
        );
    }
}
