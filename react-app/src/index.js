import React from 'react'
import ReactDOM from 'react-dom'
import {createStore} from 'redux'
import {Provider} from 'react-redux'

import 'bootstrap/dist/css/bootstrap.css'
import 'rodal/lib/rodal.css'
import './transition.css'

import MainContainer from './components/mainContainer'

const initialState = {
    lists: []
};


function tasksLists(state = initialState, action) {
    if (action.type === 'FETCH_LISTS') {
        return {
            lists: [...action.payload]
        }
    }
    else if (action.type === 'ADD_LIST') {
        return {
            lists: [
                {
                    id: 4,
                    title: 'Title',
                    unfinishedTasks: [],
                },
                ...state.lists,
            ]
        };
    }
    else if (action.type === 'DELETE_LIST') {
        let index = state.lists.findIndex(x => x.id === action.payload);
        state.lists.splice(index, 1);
        return {
            lists: [...state.lists]
        };
    }
    else if (action.type === 'UPDATE_LIST_TITLE') {
        console.log(action);
        let index = state.lists.findIndex(x => x.id === action.payload.list.id);
        console.log(index);
        state.lists[index].title = action.payload.list.title;

        return state;
    }
    return state;
}

const store = createStore(
    tasksLists,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

// store.subscribe(() => {
//   console.log('subscribe', store.getState());
// });

ReactDOM.render(
    <Provider store={store}>
        <MainContainer/>
    </Provider>
    , document.getElementById('root'));

// store.dispatch({
//   type: 'ADD_LIST',
//   payload: 'Home tasks list'
// });
//
// store.dispatch({
//   type: 'ADD_LIST',
//   payload: 'Job tasks list'
// });
